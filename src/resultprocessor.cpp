#include "../inc/resultprocessor.h"

ResultProcessor::ResultProcessor(matrix_t const & scoreMatrix,
                                 tbb::concurrent_vector<Result> & results,
                                 size_t minMatchLength, size_t iSize, size_t jSize):
    scoreMatrix_m(scoreMatrix),
    results_m(results),
    minMatchLength_m(minMatchLength),
    iSize_m(iSize),
    jSize_m(jSize)
{
}

void ResultProcessor::operator() (const tbb::blocked_range2d<size_t> & subrange) const {
    for (size_t i = subrange.rows().begin(); i < subrange.rows().end(); ++i) {
        for (size_t j = subrange.cols().begin(); j < subrange.cols().end(); ++j) {
            
            if(scoreMatrix_m[i][j] >= minMatchLength_m) {
                //this match can be shifted on i and j
                if(i + 1 < iSize_m && j + 1 < jSize_m && scoreMatrix_m[i][j] <= scoreMatrix_m[i + 1][j + 1])
                    continue;
                //this match can be shifted on j
                if(i < iSize_m && j + 1 < jSize_m && scoreMatrix_m[i][j] <= scoreMatrix_m[i][j + 1])
                    continue;
                //this match can be shifted on i
                if(i + 1 < iSize_m && j < jSize_m && scoreMatrix_m[i][j] <= scoreMatrix_m[i + 1][j])
                    continue;
                
                results_m.push_back(Result(scoreMatrix_m, i, j));
            }
        }
    }
}
