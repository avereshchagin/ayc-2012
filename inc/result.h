#ifndef RESULT_H
#define RESULT_H

#include <iostream>

#include "../inc/diagonalfiller.h"

class Result {
    
    size_t i_m;
    size_t j_m;
    
    size_t firstStart_m;
    size_t firstEnd_m;
    size_t secondStart_m;
    size_t secondEnd_m;
    
public:
    Result(matrix_t const & scoreMatrix, size_t i, size_t j);
    
    friend std::ostream & operator<<(std::ostream & os, Result const & result);
    
    class Compare {
    public:
        bool operator() (Result const & x, Result const & y) const;
    };
};

#endif // !RESULT_H
