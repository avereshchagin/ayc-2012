COMPILER ?= $(ICC_PATH)icpc

FLAGS ?= -std=c++0x -U__GXX_EXPERIMENTAL_COMPILER0X__ -xHOST -march=native -mtune=core2 -O3 -fast -w1 -finline-functions $(ICC_SUPPFLAGS) -I/Developer/opt/intel/tbb/include

LDFLAGS ?= -g -L/Developer/opt/intel/tbb/lib
LDLIBS = -ltbb -ltbbmalloc

OUTPUT = task3

SRCS := $(wildcard src/*.cpp)
HDRS := $(wildcard inc/*.h)
OBJS = $(addsuffix .o,$(basename $(SRCS)))

all: $(OUTPUT)

$(OUTPUT): $(OBJS)
	$(COMPILER) $(LDFLAGS) -o $(OUTPUT) $(OBJS) $(LDLIBS)

%.o: %.cpp $(HDRS)
	$(COMPILER) $(FLAGS) -o $@ -c $<

run: $(OUTPUT)
	export DYLD_LIBRARY_PATH=/Developer/opt/intel/tbb/lib
	./$(OUTPUT) 4 8 refseq.txt input.txt input2.txt

clean:
	rm -f $(OUTPUT)
	find . -name *.o -exec rm -f {} \;
