#ifndef APPLICATION_H
#define APPLICATION_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <string>
#include <cstdlib>
#include <tbb/task_scheduler_init.h>
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <tbb/blocked_range2d.h>
#include <tbb/partitioner.h>
#include <tbb/concurrent_vector.h>
#include <tbb/parallel_sort.h>

#include "../inc/diagonalfiller.h"
#include "../inc/result.h"
#include "../inc/resultprocessor.h"
#include "../inc/inputreader.h"

class Application {
    
public:
    Application(int argc, char *argv[]);
    
    std::pair<std::string, std::string> getSequence(std::ifstream & s);
    
    void run();
    
private:
    size_t numberOfThreads_m;
    size_t minMatchLength_m;
    std::string referenceFileName_m;
    std::vector<std::string> sequenceFileNames_m;
};

#endif // APPLICATION_H
