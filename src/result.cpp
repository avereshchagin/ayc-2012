#include "../inc/result.h"

Result::Result(matrix_t const & scoreMatrix, size_t i, size_t j):
    i_m(i),
    j_m(j)
{
    firstStart_m = i_m - scoreMatrix[i_m][j_m] + 2;
    firstEnd_m = i_m + 1;
    secondStart_m = j_m - scoreMatrix[i_m][j_m] + 2;
    secondEnd_m = j_m + 1;
}

std::ostream & operator<<(std::ostream & os, Result const & result) {
    os << result.firstStart_m << " " << result.firstEnd_m << " " <<
        result.secondStart_m << " " << result.secondEnd_m;
    return os;
}

bool Result::Compare::operator() (Result const & x, Result const & y) const {
    if (x.i_m == y.i_m) {
        return x.j_m < y.j_m;
    }
    return x.i_m < y.i_m;
}
