#include "../inc/application.h"

using std::string;
using std::stringstream;
using std::cout;
using std::endl;
using std::pair;
using std::ifstream;
using std::vector;
using std::map;
using std::min;
using std::max;

Application::Application(int argc, char *argv[]):
    numberOfThreads_m(atol(argv[1])),
    minMatchLength_m(atol(argv[2])),
    referenceFileName_m(argv[3])
{
    sequenceFileNames_m.reserve(argc - 4);
    for (int i = 4; i < argc; ++i) {
        sequenceFileNames_m.push_back(string(argv[i]));
    }
    tbb::task_scheduler_init init(numberOfThreads_m);
}

void Application::run() {
    InputReader reader(referenceFileName_m, sequenceFileNames_m);
    pair<string,string> ref = reader.getReferenceSequence();
    
	string refSeqName = ref.first;
	string refSeq = ref.second;
    
    map<string,string> otherSequences;
    pair<string,string> other;
    while (true) {
        other = reader.getOtherSequence();
        if (other.first == "") {
            break;
        }
        otherSequences[other.first] = other.second;
    }
    
	for (map<string,string>::iterator sequencesIter = otherSequences.begin(); sequencesIter !=otherSequences.end(); ++sequencesIter) {
		cout << sequencesIter->first << "\n";
		string otherSeq = sequencesIter->second;
        
        matrix_t scoreMatrix(refSeq.length());
        for (matrix_t::iterator it = scoreMatrix.begin();
             it != scoreMatrix.end(); ++it) {
            *it = vector<size_t>(otherSeq.length());
        }
        
        int sumLength = refSeq.length() + otherSeq.length();
        int minLength = min(refSeq.length(), otherSeq.length());
        int maxLength = max(refSeq.length(), otherSeq.length());
        
        DiagonalFiller filler(refSeq.length() > otherSeq.length(),
                              refSeq, otherSeq, scoreMatrix);
        for (int i = 0; i <= sumLength; ++i) {
            tbb::parallel_for(tbb::blocked_range<int>(max(i - (maxLength - 1), 0),
                                                      min(minLength - 1, i) + 1),
                              filler, tbb::auto_partitioner());
            filler.advanceDiagonal();
        }
        
        tbb::concurrent_vector<Result> results;
        ResultProcessor resultProcessor(scoreMatrix, results, minMatchLength_m,
                                        refSeq.length(), otherSeq.length());
        
        tbb::parallel_for(tbb::blocked_range2d<size_t>(0, refSeq.length(),
                                                       0, otherSeq.length()),
                          resultProcessor, tbb::auto_partitioner());
        
        tbb::parallel_sort(results.begin(), results.end(), Result::Compare());
        
        stringstream ss;
        for (tbb::concurrent_vector<Result>::iterator it = results.begin();
             it != results.end(); ++it) {
            ss << *it << endl;
        }
        cout << ss.str();
	}
}
