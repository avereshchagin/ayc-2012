#ifndef DIAGONAL_FILLER
#define DIAGONAL_FILLER

#include <string>
#include <vector>
#include <tbb/blocked_range.h>

typedef std::vector< std::vector<size_t> > matrix_t;

class DiagonalFiller {
    
    int diagonal_m;
    bool horizontal_m;
    std::string const & refSeq_m;
    std::string const & otherSeq_m;
    matrix_t & scoreMatrix_m;
    
public:
    DiagonalFiller(bool horizontal, std::string const & refSeq,
                   std::string const & otherSeq, matrix_t & scoreMatrix);
    
    void operator() (const tbb::blocked_range<int> & range) const;
    
    void advanceDiagonal();
};

#endif // !DIAGONAL_FILLER
