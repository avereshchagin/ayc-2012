#include "../inc/diagonalfiller.h"

DiagonalFiller::DiagonalFiller(bool horizontal, std::string const & refSeq,
                               std::string const & otherSeq, matrix_t & scoreMatrix):
    diagonal_m(0),
    horizontal_m(horizontal),
    refSeq_m(refSeq),
    otherSeq_m(otherSeq),
    scoreMatrix_m(scoreMatrix)
{
}

void DiagonalFiller::operator() (const tbb::blocked_range<int> & range) const {
    for (int j = range.begin(); j != range.end(); ++j) {
        size_t x = 0;
        size_t y = 0;
        if (horizontal_m) {
            x = diagonal_m - j;
            y = j;
        } else {
            x = j;
            y = diagonal_m - j;
        }
        
        if (refSeq_m[x] == otherSeq_m[y]) {
            if (x == 0 || y == 0) {
                scoreMatrix_m[x][y] = 1;
            } else {
                scoreMatrix_m[x][y] = scoreMatrix_m[x-1][y-1] + 1;
            }
        } else {
            scoreMatrix_m[x][y] = 0;
        }
    }
}

void DiagonalFiller::advanceDiagonal() {
    ++diagonal_m;
}
