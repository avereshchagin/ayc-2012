#include "../inc/inputreader.h"

InputReader::InputReader(std::string reference, std::vector<std::string> inputs):
    currentFileIndex_m(0),
    currentPosition_m(0)
{
    referenceSequence_m = mapFile(reference);
    for (std::vector<std::string>::iterator it = inputs.begin(); it != inputs.end(); ++it) {
        otherSequences_m.push_back(mapFile(*it));
    }
}

InputReader::~InputReader() {
    for (std::vector<CleanupData>::iterator it = openedFiles_m.begin();
         it != openedFiles_m.end(); ++it) {
        munmap(it->ptr, it->length);
        close(it->fd);
    }
}

mapping_t InputReader::mapFile(std::string fileName) {
    char *data = 0;
    int fd = -1;
    
    if ((fd = open(fileName.c_str(), O_RDONLY)) == -1) {
        return std::make_pair(static_cast<char *>(0), 0);
    }
    
    struct stat st;
    if (fstat(fd, &st) == -1) {
        close(fd);
        return std::make_pair(static_cast<char *>(0), 0);
    }
    
    size_t length = st.st_blocks * st.st_blksize;
    data = static_cast<char *>(mmap(NULL, length, PROT_READ, MAP_SHARED, fd, 0));
    if (data == MAP_FAILED) {
        close(fd);
        return std::make_pair(static_cast<char *>(0), 0);
    }
    
    openedFiles_m.push_back(CleanupData(fd, data, length));
    return std::make_pair(data, st.st_size);
}

std::pair<std::string, std::string> InputReader::getReferenceSequence() {
    
    int position = 0;
    char *data = referenceSequence_m.first;
    size_t length = referenceSequence_m.second;
    std::string sequence;
    std::string name;
    
    while (position < length && data[position] != '>') {
        ++position;
    }
    ++position;
    
    while (position < length && data[position] != '\n') {
        name.push_back(data[position]);
        ++position;
    }
    while (position < length && data[position] != '>') {
        char c = data[position];
        if (c == 'G' || c == 'T' || c == 'A' || c== 'C') {
			sequence.push_back(c);
		}
        ++position;
    }
    
    return std::make_pair(name, sequence);
}

std::pair<std::string, std::string> InputReader::getOtherSequence() {
    if (currentFileIndex_m < otherSequences_m.size() &&
        currentPosition_m >= otherSequences_m[currentFileIndex_m].second) {
        ++currentFileIndex_m;
        currentPosition_m = 0;
    }
    if (currentFileIndex_m >= otherSequences_m.size()) {
        return std::make_pair("", "");
    }
    
    char *data = otherSequences_m[currentFileIndex_m].first;
    size_t length = otherSequences_m[currentFileIndex_m].second;
    std::string sequence;
    std::string name;
    
    while (currentPosition_m < length && data[currentPosition_m] != '>') {
        ++currentPosition_m;
    }
    ++currentPosition_m;
    
    while (currentPosition_m < length && data[currentPosition_m] != '\n') {
        name.push_back(data[currentPosition_m]);
        ++currentPosition_m;
    }
    while (currentPosition_m < length && data[currentPosition_m] != '>') {
        char c = data[currentPosition_m];
        if (c == 'G' || c == 'T' || c == 'A' || c== 'C') {
			sequence.push_back(c);
		}
        ++currentPosition_m;
    }
    
    return std::make_pair(name, sequence);
}
