#ifndef RESULTPROCESSOR_H
#define RESULTPROCESSOR_H

#include <tbb/blocked_range2d.h>
#include <tbb/concurrent_vector.h>

#include "../inc/diagonalfiller.h"
#include "../inc/result.h"

class ResultProcessor {
    
    matrix_t const & scoreMatrix_m;
    tbb::concurrent_vector<Result> & results_m;
    size_t minMatchLength_m;
    size_t iSize_m;
    size_t jSize_m;
    
public:
    ResultProcessor(matrix_t const & scoreMatrix,
                    tbb::concurrent_vector<Result> & results,
                    size_t minMatchLength, size_t iSize, size_t jSize);
    
    void operator() (const tbb::blocked_range2d<size_t> & subrange) const;
};

#endif // !RESULTPROCESSOR_H
