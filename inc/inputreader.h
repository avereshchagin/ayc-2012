#ifndef INPUTREADER_H
#define INPUTREADER_H

#include <string>
#include <vector>
#include <utility>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

typedef std::pair<char *, size_t> mapping_t;
typedef std::vector<mapping_t> mappings_t;

struct CleanupData {
    int fd;
    char *ptr;
    size_t length;
    CleanupData(int fd, char *ptr, size_t length):
        fd(fd), ptr(ptr), length(length) {}
};

class InputReader {
    
public:
    InputReader(std::string reference, std::vector<std::string> inputs);
    
    ~InputReader();
    
    std::pair<std::string, std::string> getReferenceSequence();
    
    std::pair<std::string, std::string> getOtherSequence();
    
private:
    mapping_t referenceSequence_m;
    mappings_t otherSequences_m;
    std::vector<CleanupData> openedFiles_m;
    
    int currentFileIndex_m;
    int currentPosition_m;
    
    mapping_t mapFile(std::string fileName);
};

#endif // !INPUTREADER_H
